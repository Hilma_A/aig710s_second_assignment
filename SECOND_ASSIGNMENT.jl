### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ e60b2f2f-d6fc-47eb-9f84-22c61b684c92
using Flux: onehotbatch, crossentropy, onecold, @epochs

# ╔═╡ e74e46dc-95a8-468a-86a8-bd2209f9f0c5
using MLDataUtils

# ╔═╡ e71a6228-916a-4130-95a8-552d06c8b795
using Base.Iterators: partition, repeated

# ╔═╡ 0be98df1-2f95-465e-bfaf-74d6b7853304
using BSON: @save, @load

# ╔═╡ 72132544-1b1a-458b-95e4-7a87642ae733
using Images

# ╔═╡ 7da59b43-8735-499e-a5ea-3cd58090c320
using Flux

# ╔═╡ 5229af0f-0b62-4487-b501-a35c3588f84f
using Random

# ╔═╡ 842a267d-05e8-4d32-8323-7a6640099d91
using Statistics

# ╔═╡ e901016d-ae80-452f-bed1-62e35d1e958a
using Printf

# ╔═╡ 27af06b8-2a46-4c19-83fb-70e10100204d
md"# Artificial Intelligence and Computer Graphics
### AIG710S SECOND ASSIGNMENT"

# ╔═╡ 3b43d1b9-4674-4dfd-9656-88b67682aafd
md"
- This assignment aims to build an image classifier using a convolutional neural network (CNN). 
- The images, accessible at https://www.kaggle.com/ paultimothymooney/chest-xray-pneumonia, represent chest X-ray images of patients. 
- Two possible types of labels are considered: normal and pneumonia. Put another way, each sample represents either a normal patient or a patient suffering from pneumonia. "

# ╔═╡ 229421e0-d070-11eb-0cc3-2df2bbdcecdf
md"#### Testing Resizing one image"


# ╔═╡ e0209ae3-ed0a-4aa8-bc37-613e723b599d
resolution = 200

# ╔═╡ 001d13c8-abd3-4cac-a777-ef2d5962a781
resized_gray_img = Gray.(load(pwd() * "/archive/chest_xray/test/NORMAL/IM-0001-0001.jpeg")) |> (x -> imresize(x, resolution, resolution))

# ╔═╡ bd103804-86d7-4e8b-9cdd-d7e7ffa0268e
md"function **resize_and_grayify** will create new folders where the images are resized"


# ╔═╡ 3e23aea9-0633-42ff-baa5-30b6b5f72002
function resize_and_grayify(directory, image_name, width::Int64, height::Int64)
    resized_gray_img = Gray.(load(directory * "/" * image_name)) |> (x -> imresize(x, resolution, resolution))
    try
        save(directory * "_preprocessed/" * image_name, resized_gray_img)
    catch e
        if isa(e, SystemError)
            mkdir("preprocessed_" * directory)
            save("preprocessed_" * directory * "/" * image_name, resized_gray_img)
        end
    end
end

# ╔═╡ fe1b2050-b774-4af6-9816-145e309202bf
function process_images(directory, width::Int64, height::Int64)
    files_list = readdir(directory)
    map(x -> resize_and_grayify(directory, x, width, height),files_list)
end

# ╔═╡ 03e25108-c493-42ca-81f2-fff85e4edbb5
md"###### Folder before preprocessing"

# ╔═╡ 39848c0f-53cb-42a0-8129-534bc33205e8
load( pwd() * "/before_preprocess.PNG")

# ╔═╡ 3453b4fd-96b6-456b-bb37-01c355c82a3a
md"###### Preprocessing Starts"

# ╔═╡ a6a365d4-4ca8-4d85-a3f8-72f0d889b9d5
begin
	process_images(pwd() * "/archive/chest_xray/test/NORMAL", resolution, resolution)
	process_images(pwd() * "/archive/chest_xray/val/NORMAL", resolution, resolution)
	process_images(pwd() * "/archive/chest_xray/test/PNEUMONIA", resolution, resolution)
	process_images(pwd() * "/archive/chest_xray/val/PNEUMONIA", resolution, resolution)
	
end

# ╔═╡ d75ad366-e5f0-402f-b019-9c761f5dcd5d
md"###### Folder After preprocessing
Two new folders are created"

# ╔═╡ f571d0d6-7ce9-4ad3-876b-9cf5955d62f0
load( pwd() * "/after_preprocess.PNG")

# ╔═╡ 7f442338-6d52-49a0-a610-633de5e7f56f
md"###### Load the preprocessed folders"

# ╔═╡ 167b2279-cd6a-4110-8537-8b3aec86fae8
function holdOut(Patterns::Int, percentageTest::Float64)
    @assert ((percentageTest>=0.) & (percentageTest<=1.));
    indices = randperm(Patterns);
    trainingPatterns = Int(round(Patterns*(1-percentageTest)));
    return (indices[1:trainingPatterns], indices[trainingPatterns+1:end]);
end

# ╔═╡ 2c72dbbb-eb9f-43cc-9b3b-b75dd79fd008
function load_(dir, label)
    directory= readdir(dir);
    pattern = Array{Array{RGB{Normed{UInt8,8}},2}}(undef, 0)
    labelS = zeros(size(directory,1))
    i=1;
    for file in directory
        img = load(string(dir,"/",file));
        
        imgR = imresize(img, (resolution, resolution));
        push!(pattern, imgR);
        labelS[i] = label;
        i = i + 1;
    end
    return (pattern, labelS);
end

# ╔═╡ 835e7c0a-492c-4c6f-81cf-298e73b9bac3
function patterns(pat, res, gTest)
    x_train = Array{Array{Float32,3}}(undef, 0)
    test = Array{Array{Float32,3}}(undef, 0)
    
    numP = size(pat,1);
    
    (lEntre, lTest) = holdOut(numP, gTest)

    y_train = Array{Int64,1}(undef, 0)
    rt =Array{Int64,1}(undef, 0)
    
    for i = 1:size(lEntre,1)
        push!(x_train, permutedims(channelview(pat[lEntre[i]]),[2,3,1]))
        push!(y_train, res[lEntre[i]])
    end
    for i = 1:size(lTest,1)
        push!(test, permutedims(channelview(pat[lTest[i]]),[2,3,1]))
        push!(rt, res[lTest[i]])
    end
    
    return (x_train,y_train,lEntre),(test,rt,lTest);
end

# ╔═╡ c46333cc-bb25-4ec6-ac70-e12727ec3137
function create3D(m1, m2, r1, r2)
    p = vcat(m1,m2)
    r = vcat(r1,r2)
    return (p,r)
end

# ╔═╡ 1fa5fd5f-3b5d-4932-8353-04eeb29333a9
begin
	(A, B) = load_(pwd() * "/archive/chest_xray/test/NORMAL", 1.);
	(A_, B_) = load_(pwd() * "/archive/chest_xray/test/PNEUMONIA", 0.);
	(normal_,pneumonia_) = create3D(A, A_, B, B_);
	(pF_normal, sF_normal) = load_(pwd() * "/archive/chest_xray/val/NORMAL", 1.);
	(normal,pneumonia) = create3D(normal_, pF_normal, pneumonia_, sF_normal);	
end

# ╔═╡ 2bde4838-51fd-42b4-83a3-a8e4c99a63c8
(x_train,y_train,le),(pt,rt,lt) = patterns(normal,pneumonia,0.1);

# ╔═╡ 6b9f0be1-7c5b-4cf5-a38f-698c3f4cda50
md"###### This function creates minibatches of the data. Each minibatch is a tuple of (data, labels)."

# ╔═╡ bc6d74a3-8b4d-477c-a811-f2b1795c4635
function make_minibatch(X, Y, idxs)
    X_batch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        X_batch[:, :, :, i] = Float32.(X[idxs[i]])
    end
    Y_batch = onehotbatch(Y[idxs], 0:1)
    return (X_batch, Y_batch)
end

# ╔═╡ 2a03fbe1-6266-4dae-a72e-ce5d3d6e021f
md"###### Here we define the train and test sets."

# ╔═╡ a5198ff4-e823-4828-86bc-e8c7e84c9f9b
batchsize = 128

# ╔═╡ 10c2fa14-9233-489f-8967-f46624620402
mb_idxs = partition(1:length(x_train), batchsize)

# ╔═╡ 63350650-053a-47c4-b48e-97899a529714
begin
	train_set = [make_minibatch(x_train, y_train, i) for i in mb_idxs]
	test_set = make_minibatch(x_test, y_test, 1:length(x_test));
end;

# ╔═╡ 4a1fffc6-3576-4ef6-9932-72ef561c71ee
md"## 2. The Model"

# ╔═╡ d0541c7b-8712-4ae6-ade7-8ab2465edd96
md"###### LeNet5 constructor below"

# ╔═╡ 9c1f2e34-3c1e-4951-b804-c908764f3fa5
function LeNet5()
	return Chain(
    	Conv((3, 3), 3=>16, pad=(1,1), relu),
    	MaxPool((2,2)), #maxpooling
    	Conv((3, 3), 16=>32, pad=(1,1), relu),
    	MaxPool((2,2)), #maxpooling
    	Conv((3, 3), 32=>32,pad=(1,1), relu),
    	MaxPool((2,2)),
    	x -> reshape(x, :, size(x, 4)),
    	Dense(20000,2),
    	softmax,
	)
end

# ╔═╡ 119b9dd1-2869-4525-9ef1-806aab9ac5be
model = LeNet5()

# ╔═╡ 3d3f1161-18e7-496e-8534-5fc8a3d5d7e7
md"###### Utility functions"

# ╔═╡ 97b0a64f-9223-45ab-b9cc-5789d5f98f6d
begin
	num_params(model) = sum(length, Flux.params(model)) 
	round4(x) = round(x, digits=4)
end

# ╔═╡ 810027b2-54cc-41fb-b423-8018d706d282
begin
	train_loss = Float64[]
	test_loss = Float64[]
	acc = Float64[]
	ps = Flux.params(model)
	opt = ADAM()
	L(x, y) = Flux.crossentropy(model(x), y)
	L((x,y)) = Flux.crossentropy(model(x), y)
	accuracy(x, y, f) = mean(Flux.onecold(f(x)) .== Flux.onecold(y))
	
	function update_loss!()
	    push!(train_loss, mean(L.(x_train)))
	    push!(test_loss, mean(L(y_train)))
	    push!(acc, accuracy(test_set..., model))
	    @printf("TRAIN LOSS : %.2f\n, TEST LOSS : %.2f\n, ACCURACY : %.2f\n", train_loss[end], test_loss[end], acc[end])
	end
end

# ╔═╡ aa372d57-9221-478b-897d-42cbd720328d
md"###### Start training our neural network"

# ╔═╡ aeb77209-0d17-421c-bfe8-fec8fdc5c23a
@epochs 2 Flux.train!(L, ps, x_train, opt;
               cb = Flux.throttle(update_loss!, 8))

# ╔═╡ fbce6bad-f4b8-4ea9-aa38-8c445d3b7570
begin
	plot(train_loss, xlabel="LOOPS", title="Model Performance", label="Train loss", lw=3, alpha=0.9)
	plot!(test_loss, label="TEST LOSS", lw=3, alpha=0.9)
	plot!(acc, label="ACCURACY", lw=3, alpha=0.9)
end

# ╔═╡ 28563932-0066-416b-8199-d11f8a92cd89
begin
	Flux.testmode!(model,false)   # enable dropout
	@epochs 5 Flux.train!(L, ps, train_set, ADAM(0.0005);cb = Flux.throttle(update_loss!, 8))
	Flux.testmode!(model,true)    # disable dropout
end

# ╔═╡ 556c3c39-7047-4670-8aba-c0614443cbe0
begin
	plot(train_loss, xlabel="LOOPS", title="Model Performance", label="Train loss", lw=3, alpha=0.9)
	plot!(test_loss, label="TEST LOSS", lw=3, alpha=0.9)
	plot!(acc, label="ACCURACY", lw=3, alpha=0.9)
	vline!([82], lw=2, label=false)
end

# ╔═╡ 2ac9b5f9-b044-4bfd-8e26-e3f86e6d53c4
md"### Save Model"

# ╔═╡ 59e16798-8cf5-47c5-a385-7d5f90e982a0
@save "mymodel.bson" model

# ╔═╡ 04d62dde-267b-4a4a-9e4b-f7a9d9d677bd
@load "mymodel.bson" model

# ╔═╡ Cell order:
# ╠═e60b2f2f-d6fc-47eb-9f84-22c61b684c92
# ╠═e74e46dc-95a8-468a-86a8-bd2209f9f0c5
# ╠═e71a6228-916a-4130-95a8-552d06c8b795
# ╠═0be98df1-2f95-465e-bfaf-74d6b7853304
# ╠═72132544-1b1a-458b-95e4-7a87642ae733
# ╠═7da59b43-8735-499e-a5ea-3cd58090c320
# ╠═5229af0f-0b62-4487-b501-a35c3588f84f
# ╠═842a267d-05e8-4d32-8323-7a6640099d91
# ╟─27af06b8-2a46-4c19-83fb-70e10100204d
# ╟─3b43d1b9-4674-4dfd-9656-88b67682aafd
# ╟─229421e0-d070-11eb-0cc3-2df2bbdcecdf
# ╠═e0209ae3-ed0a-4aa8-bc37-613e723b599d
# ╠═001d13c8-abd3-4cac-a777-ef2d5962a781
# ╟─bd103804-86d7-4e8b-9cdd-d7e7ffa0268e
# ╠═3e23aea9-0633-42ff-baa5-30b6b5f72002
# ╠═fe1b2050-b774-4af6-9816-145e309202bf
# ╟─03e25108-c493-42ca-81f2-fff85e4edbb5
# ╟─39848c0f-53cb-42a0-8129-534bc33205e8
# ╟─3453b4fd-96b6-456b-bb37-01c355c82a3a
# ╠═a6a365d4-4ca8-4d85-a3f8-72f0d889b9d5
# ╟─d75ad366-e5f0-402f-b019-9c761f5dcd5d
# ╟─f571d0d6-7ce9-4ad3-876b-9cf5955d62f0
# ╟─7f442338-6d52-49a0-a610-633de5e7f56f
# ╠═167b2279-cd6a-4110-8537-8b3aec86fae8
# ╠═2c72dbbb-eb9f-43cc-9b3b-b75dd79fd008
# ╠═835e7c0a-492c-4c6f-81cf-298e73b9bac3
# ╠═c46333cc-bb25-4ec6-ac70-e12727ec3137
# ╠═1fa5fd5f-3b5d-4932-8353-04eeb29333a9
# ╠═2bde4838-51fd-42b4-83a3-a8e4c99a63c8
# ╟─6b9f0be1-7c5b-4cf5-a38f-698c3f4cda50
# ╠═bc6d74a3-8b4d-477c-a811-f2b1795c4635
# ╟─2a03fbe1-6266-4dae-a72e-ce5d3d6e021f
# ╠═a5198ff4-e823-4828-86bc-e8c7e84c9f9b
# ╠═10c2fa14-9233-489f-8967-f46624620402
# ╠═63350650-053a-47c4-b48e-97899a529714
# ╟─4a1fffc6-3576-4ef6-9932-72ef561c71ee
# ╠═d0541c7b-8712-4ae6-ade7-8ab2465edd96
# ╠═9c1f2e34-3c1e-4951-b804-c908764f3fa5
# ╠═119b9dd1-2869-4525-9ef1-806aab9ac5be
# ╟─3d3f1161-18e7-496e-8534-5fc8a3d5d7e7
# ╠═97b0a64f-9223-45ab-b9cc-5789d5f98f6d
# ╠═e901016d-ae80-452f-bed1-62e35d1e958a
# ╠═810027b2-54cc-41fb-b423-8018d706d282
# ╟─aa372d57-9221-478b-897d-42cbd720328d
# ╠═aeb77209-0d17-421c-bfe8-fec8fdc5c23a
# ╠═fbce6bad-f4b8-4ea9-aa38-8c445d3b7570
# ╠═28563932-0066-416b-8199-d11f8a92cd89
# ╠═556c3c39-7047-4670-8aba-c0614443cbe0
# ╠═2ac9b5f9-b044-4bfd-8e26-e3f86e6d53c4
# ╠═59e16798-8cf5-47c5-a385-7d5f90e982a0
# ╠═04d62dde-267b-4a4a-9e4b-f7a9d9d677bd
